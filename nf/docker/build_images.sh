
#### build docker images
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/bwa:latest -f docker/bwa.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/cutadapt:latest -f docker/cutadapt.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/featurecounts:latest -f docker/featurecounts.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/macs2:latest -f docker/macs2.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/rseqc:latest -f docker/rseqc.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/samplot:latest -f docker/samplot.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/scanalysis:latest -f docker/scanalysis.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/seurat:latest -f docker/seurat.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/skbio:latest -f docker/skbio.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/star:latest -f docker/star.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/umi_tools:latest -f docker/umi_tools.dockerfile .
docker image build -t 204154409870.dkr.ecr.us-west-2.amazonaws.com/bamtofrag:latest -f docker/bamtofrag.dockerfile .


