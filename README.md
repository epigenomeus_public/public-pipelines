# pipelines

Repository for Paired-Tag data processing pipeline

# Required pre-processed data

For a target genome, the following files need to be downloaded and processed:
  1) Genomic fasta
  2) Transcriptomic GTF
  3) Annotation SAF files (promoter, enhancer, heterochromatin, genic)
  4) STAR index
  5) BWA index

Example:

```
/home/share/storages/2T/genome/mouse/
├── GRCm39.primary_assembly.genome.fa.gz           # genome fasta file
├── bwa_index                                      # bwa index (for DNA)
│   ├── GRCm39.primary_assembly.genome.fa.fai
│   ├── GRCm39.primary_assembly.genome.fa.gz
│   ├── [...]
├── gencode.vM28.annotation.gtf                    # transcriptome GTF
├── gencode.vM28.annotation.collapsed.gtf          # collapsed transcriptome GTF (for RNAseQC)
├── gencode.vM28.annotation.saf                    # transcriptome SAF (for FeatureCounts)
├── 20220603-mm39-brain-promoter-annotated.saf     # putative promoter SAF
├── 20220603-mm39-brain-enhancer-map-annotated.saf # putative enhancer SAF
├── 20220603-mm39-brain-heterochromatin.saf        # putative heterochromatin SAF
├── GRCm39_5kb.saf                                 # genomic bin SAF
└── star_index                                     # STAR index of transcriptome GTF
    ├── chrLength.txt
    ├── chrNameLength.txt
    ├── [...]
```

The files will then need to be registered in `nf/workflows/prod_pipeline.nf` under the appropriate section, for instance:

```
[...]
params.genome_saf_file = [
  "hs": file(params.GENOME_DIR + "/human/gencode.v39.annotation.saf"),    // change to reflect your filepath 
  "mm": file(params.GENOME_DIR + "/mouse/gencode.vM28.annotation.saf" )   // change to reflect yoru filepath
]

params.genome_gtf_collapsed_file = [
  "hs": file(params.GENOME_DIR + "/human/gencode.v39.annotation.collapsed.gtf"), // etc
  "mm": file(params.GENOME_DIR + "/mouse/gencode.vM28.annotation.collapsed.gtf")
]
[...]
```

# Spike-ins

Most runs of Paired-Tag are performed with cell line spike-ins from another species. These serve
as a positive control for RNA and DNA quality, and so the production pipeline is set up to
use both a primary species and a spikein species, and to assign barcodes to 'primary', 'spikein',
or 'ambiguous' categories on the basis of alignment rate to the different genome/transcriptomes.

Legacy (non-spikein) data can use `prod_pipeline_nospikein.nf` which in combination with `--NO_SPIKEIN 'yes'` to
skip the spikein analysis, and force all reads to be assigned to the primary species.

# Running the pipeline
## Local docker container env
### No spikein example
nextflow run ~/repos/pipelines/nf/workflows/prod_pipeline_nospike.nf   
-c ~/repos/pipelines/nf/workflows/config/prod_nextflow.config   
--RUN_NAME 'docker_v3_nosplikein_070192023'   
--LIBRARY_DIGEST_FILE '20221108_xxx_MiniSeq_LibraryDigest.csv'   
--SAMPLE_DIGEST_FILE '20221108_xxx_MiniSeq_SampleDigest.csv' 
--SPECIES 'mm' 
--SPIKEIN_SPECIES 'hs'
--output_dir 'publisher_xxx'  
--NO_SPIKEIN 'yes' 

### Spikein example

ex1.
nextflow run ~/repos/pipelines/nf/workflows/prod_pipeline.nf   
-c ~/repos/pipelines/nf/workflows/config/prod_nextflow.config   
--RUN_NAME 'dockerV3_spikein_R43_AD_May30' 
--LIBRARY_DIGEST_FILE 'R43_May30.library_digest.csv'   
--SAMPLE_DIGEST_FILE 'R43_May30.sample_digest.csv' 
--NO_SPIKEIN 'no' 
--SPECIES 'hs' 
--SPIKEIN_SPECIES 'mm'    
--output_dir 'publisher_xxx'

ex2.
nextflow run ~/repos/pipelines/nf/workflows/prod_pipeline.nf -c ~/repos/pipelines/nf/workflows/config/prod_nextflow.config --RUN_NAME '20230825_XXXX_PT_03132023_2' --LIBRARY_DIGEST_FILE '20230825_XXXX_PT_03132023_2_library_digest.csv' --SAMPLE_DIGEST_FILE '20230825_XXXX_PT_03132023_2_sample_digest.csv' --GENOME_DIR '/home/share/storages/2T/genome/' --NO_SPIKEIN 'no' --SPECIES 'rat' --SPIKEIN_SPECIES 'hs' --TEST_RUN 'no' --output_dir 'publisher_XXXX_08292023_2'   


# Pipeline Overview

Inputs:

1) A library digest file, giving for each library the associated library type, lysis ID and fastQ path (e.g., SDA3,A3,dna,/path/to/SDA3_R1.fq,/path/to/SDA3_R2.fq)

2) A sample digest file, given for each library the 12 assays performed, their corresponding sample ID and antibody target. For safety the library fastQ files are also included.

3) The primary and spike-in species (both are necessary even for legacy runs)

## Split Paired-Tag

Each library fastQ file is split into 13 parts, one for each underlying assay, and a final part for reads that could not be assigned to any assay.

For each read pair:
  + Known linker sequences are aligned to R2 to establish the position of the cell and assay barcodes
  + The barcodes are decoded, and any matching UMI+cell+assay+sample+antibody IDs are assigned to the read 
  + The RE sequence is checked to establish the molecule type (rna, dna, or unk)
  + The decoded and raw barcodes are added to the read name
  + R1 is written out with the read name
    + If any sequence remains after the RE site in R2 (>100bp), it is written out with the same name
    + If no sequence remains after the RE site, "N" is written into the R2 file with the same read name

A barcode QC file is produced to provide information on the # of properly decoded
barcodes, and the % of reads that are matching the library type (via the RE sequence),
versus the % of reads that do not ("contaminants").

## Alignment

### DNA

FastQ files are filtered to retain only reads assigned as "dna" via their RE sequence. Illumina adapter sequences are trimmed.

The R1 file is inspected. If the average R1 read length is >140, then R2 will contain 20-50bp of genomic information,
and both R1 and R2 are used to align to the genome (bwa) in paired-end mode. Otherwise only R1 is aligned (bwa).

Reads are aligned both to the primary and spike-in genome.

### RNA

The R1 fastQ file is filtered to retain only reads assigned as "rna" via their RE sequence. Illumina adapter sequences are trimmed.
R1 is aligned using STAR.

Reads are aligned both to the primary and spike-in genome.


### Spike-in assignment

The full barcode (assay + UMI) are used to count the number of primary-aligned and spikein-aligned UMI for each barcode.
Barcodes are assigned PRIMARY (>70% UMI align to primary); SPIKE (>70% UMI align to spike-in); or AMBIGUOUS.

If `--NO_SPIKEIN='yes'` then all barcodes are written to both primary .bam and spike-in .bam. 

If `--NO_SPIKEIN='no' (default)` then PRIMARY + AMBIGUOUS are written to the primary .bam, and SPIKE + AMBIGUOUS are written to the spike-in bam. In addition, reads coming from an ambiguous barcode are tagged with "|ambig" in the read name.


## Peak Calling [DNA]

Primary bam files are grouped by the antibody target, and MACS2 is run in treatmeant-only mode for both narrow and broad peaks. The resulting peaks are merged and sorted. This generates one peak .bed file per antibody.

## Bam Tagging

### DNA

A custom python script (py/add_tags.py) is used to add tags to reads depending on whether the overlap an annotation interval. The tags are:
   + Genomic bins (tag: BN)
   + Enhancers (tag: EE)
   + Promoters (tag: EP)
   + Genes (tag: GN)
   + Peaks (tag: PK)

Note that peaks from each antibody are concatenated, so that each bam file is quantified and annotated with every peak from every antibody.

In addition, at this stage cell barcodes and UMI are copied from the read name into the CB and MI tags respectively. A counter (XX) is used to assign a unique ID for each read.

### RNA

A custom python script (py/add_tags.py) is used to add tags to reads depending on whether the overlap an annotation interval. The tags are:
   + Genes (tags: GN)
   + Bins (tags: BN)

In addition, at this tage cell barcodes and UMI are copied from the read name into the CB and MI tags respectively. A counter (XX) is used to assign a unique ID for each read.

## UMI Quantification

UMItools count is used to quantify UMI for each element within each tag.

DNA: BN, PK

RNA: GN

To quantify cellular UMI, CB+MI are used for cell barcodes and identifiers. To quantify cellular reads, CB+XX are used.
